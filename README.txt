Die den Hauptinhalt tragende Datei ist

    karten/content/awesome.tex



Zum Kompilieren begebe man sich in den Ordner

    karten

und führe dort ein

    ./make

aus.



Zum Aufräumen begebe man sich in den Ordner

    karten

und führe dort ein

    ./make clean

aus.



Die im Ordner

    karten/setup

liegenden Dateien sind tabu, sofern man nicht genau weiß, was man
tut.

Man beachte außerdem die Datei

    karten/src.txt

sowie, zur Verwendung der LaTeX-Klasse, die Datei

    karten/setup/manual.pdf



tdu, 2017-04-05
