#!/bin/bash

# dieses Skript kompiliert den aktuellen Stand
# und veröffentlicht ihn

# TODO: entscheiden, ob das tatsächlich passieren
# soll, daher zunächst:

exit 0

# tdu, 2020-03-11

########################## old stuff ##########################
# "tgi" in "c" umbenannt

main() {

    checkuser

    checkmachine

    tmpdir="$(maketempdir)"
    tmpdirparent="$(dirname "$tmpdir")"

    web='/home/tdu/public_html/c-lernkarten/'

    debug "$tmpdir"

    
    fcd ..                     && copytotemp
    
    fcd "$tmpdir"/karten       && cleanup           && buildpdf

    fcd "$tmpdir"              && targz             && publishfiles

    fcd "$web"                 && update-index-html

    fcd "$tmpdirparent"        && finish

    echo done
}

##########################################################################

debug () { echo "$1" >&2; }

fail  () {

    local c

    if [ -z "$1" ]; then
        echo "unspecified error" >&2
        exit 1
    fi
    
    if [ -z "$2" ]; then
        printf '>>>>>> %s\n>>>>>> cwd: %s\n\n' "$1" "$(pwd)" >&2
        exit 1
    fi

    if [ "$1" -le '0' ] || [ "$1" -ge '126' ]
    then
        c=1
    else
        c="$1"
    fi
    # if $1 is not a number, the script dies

    printf '>>>>>> %s\n>>>>>> cwd: %s\n\n' "$2" "$(pwd)" >&2
    exit "$c"
}

##########################################################################

maketempdir() {

    local result

    if ! result=$(mktemp -d); then        
        fail 'Cannot create temp directory'
    else
        echo "$result"
    fi
}

fcd() {

    # Diese Funktion erlaubt das komfortable Springen zu
    # Verzeichnissen. Sie existiert, damit in der
    # main() keine subshells gestartet werden müssen, die das
    # Abbrechen des Skriptes mittels 'exit' unmöglich (oder nur unter
    # unsauberen Verrenkungen möglich) machen würden.
    
    if [ 1 -ne "$#" ]; then
        fail 'Needs exactly one argument: the directory to change to'
    fi

    local targetdir

    targetdir="$(readlink -e "$1")" # doppelte Slashes etc. bereinigen
    if ! cd "$targetdir"; then
        debug 'I want to cd to '"$targetdir"
        fail 'Cannot change to '"$1"
    fi

}

checkuser() {
    if [ ! "$(whoami)" == 'tdu' ]
    then
        fail 'You are not tdu.\nYou are sadly out of luck.'
    fi
}

checkmachine() {
    local uniserver
    local numserv
    uniserver=(elendil gimli beorn bombadil eomer
               legolas boromir isildur denethor)
    numserv="${#uniserver[@]}"

    local ok
    local machine
    machine="$(hostname)"
    ok=false

    for ((i = 0; i < numserv; i++)); do
        if [ "${uniserver[$i]}" == "$machine" ]; then
            ok=true
            break
        fi
    done

    if [ ! "$ok" == 'true' ]
    then
        fail "You are not working on "\
             "a CAU server.\nYou are sadly out of luck."
    fi
}

copytotemp() {
    if ! cp -a karten "$tmpdir"
    then
        fail 'could not copy karten to temp directory '"$tmpdir"
    fi

    if ! cp -a src/src.txt "$tmpdir"/karten/src.txt
    then
        fail 'could not copy src/src.txt to temp directory '"$tmpdir"/karten/src.txt
    fi

    if ! cp -a README.txt "$tmpdir"/README.txt
    then
        fail 'could not copy README.txt to temp directory '"$tmpdir"
    fi
}

cleanup() {
    if ! ./make clean; then
        fail 'make clean failed'
    fi

    if ! rm -vrf archive.sh; then
        fail 'could not clean up'
    fi
}

targz() {

    if ! tar --create --posix --verbose --preserve-permissions\
         --file=./c-lernkarten.tar karten README.txt
    then
        fail 'could not create tar archive'
    fi
    sleep 0.5
    if ! gzip ./c-lernkarten.tar; then
        fail 'could not compress tar archive'
    fi
    sleep 0.5
}

buildpdf() {
    echo Making pdf...
    if ! ./make; then
        fail 'could not build pdf'
    fi
    sleep 0.5

    if ! mv c-lernkarten-master.pdf ../c-lernkarten.pdf; then
        fail 'could not move pdf out of the way'
    fi
    sleep 0.5

    if ! ./make clean; then
        fail 'make clean failed'
    fi
    sleep 0.5
}

publishfiles() {
    if ! cp -a c-lernkarten.{pdf,tar.gz} "$web"
    then
        fail 'could not copy pdf and tar.gz to public_html'
    fi
}

update-index-html() {
    local fn
    fn="$tmpdir"'/index.html.new'
    local lines
    lines=$(wc -l < index.html)
    #
    # Man beachte die Eingabeumleitung.
    # Sie sorgt dafür, dass in der Ausgabe von wc lediglich die
    # gesuchte Zeilenzahl erscheint und nicht der Dateiname.
    #
    lines=$((lines - 3))
    cp -a index.html "$fn"

    (
        head -n "$lines" index.html
        printf '%s' '<!-- hhmts start -->Last modified: '
        printf '%s' "$(date '+%a %b %e %T %Z %Y')"
        echo '<!-- hhmts end -->'
        echo '</body>'
        echo '</html>'

    ) > "$fn"

    yes | cp -af "$fn" ./index.html
}

finish() {
    if ! \rm -rf "$tmpdir"; then
        fail 'could not remove temp directory '"$tmpdir"
    fi
}

main "$@"
